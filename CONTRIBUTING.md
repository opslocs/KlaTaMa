# Mitarbeiteranleitung

Wilkommen Einsteiger! Um euch bei eurer Arbeit ein wenig zu unterstützen, wurde ein kleiner Leitfaden erstellt.
Mit diesem seid ihr bestimmt in der Lage euch zurechtzufinden.
Ich habe die Sachen in mehrere Bereiche unterteilt, um etwas Strucktur hinein zu bekommen.
Guckt es euch unbedingt an!Willst du dein neugewonnenes Wissen testen? Dann nutze doch die [Testaufgabe](#35). Viel Spass!

## Einloggen
Es gibt einen FSR-Account auf GitLab: Oben rechts kannst du dich einloggen mit dem Nutzernamen fsrmathe und dem Standardpasswort.
Einige Dinge kannst du erst dann machen, wenn du eingeloggt bist.

## Kenne dein Werkzeug

### [Das Aufgabenbrett (Issue Board)](https://gitlab.com/killer2alex/KlaTaMa/boards)
![Video](/docs/Aufgabenbrett.mp4)

Eine kleine Zusammenfassung:
- Aufgabenbrett besteht aus der Suchleiste und dem eigentichen Brett
- Das Brett selbtst besteht aus 5 Berreichen(Listen)
    - Backlog: Alle unverteilten Aufgaben
    - To Do: Alle Aufgaben, die als nächstes zu tun sind und an denen gerade keiner arbeitet
    - Doing: Alle Aufgaben, an deren Fertigstellung gerade gearbeitet wird
    - blockiert: Alle Aufgaben, die gerade blockiert sind 
    - Closed: Alle abgeschlossenen Aufgaben
- Aufgaben werden in Abhängigkeit von ihrem Status in die Listen bewegt

### Eine Aufgabe (Issue)
![Video](/docs/Aufgaben.mp4)

Eine kleine Zusammenfassung:

![Bild](/docs/Aufgabe.png)
1. Titel und Beschreibung der Aufgabe
2. auflistung aller bisherigen Aktivitäten
3. Kommentarfenster
4. Enddatum
5. Labels

### Labels
![Video](/docs/Label.mp4)

Eine kleine Zusammenfassung:

- Labels helten dabei Aufgaben zuzuordnen und zu strukturieren
- Sie lassen sich in 4 Bereiche unteteilen:
    1. Arbeitsbereich
        - Kernveranstaltung
        - Spezialveranstaltung
        - Organisation
        - Planung
        - Freizeit
    2. Status
        - To Do
        - Doing
        - Blocked
        - ofD (offen für Diskussion)
        - Closed
    3. Priorität
        - p1 (hohe Priorität)
        - p2 (mittlere Priorität)
        - p3 (geringe Priorität)
    4. Person (jeder hat sein eigenes + ~Alle wenn es für alle ist)

- Labels bilden das Rückrat unseres Organisationssystems

### Schreiben

![Video](/docs/schreiben_basic.mp4)

kleine Zusammenfassung:

- Anzeigen, dass ich schreibe mittels `~"Person"`
- nutzen von fett, kursiv Aufzählung und Aufgaben

![Viedo](/docs/schreiben_advanced.mp4)

kleine Zusammenfassung:
- Label zuordnen mittels `/label "label"`
- Label entfernen mittels `/unlabel"label"`
- Andere Aufgaben referenzieren mittels `#"Aufgabenummer"`
- Alex (Veranstaltung und Konzept) dazurufen mittels `@killer2alex`
- Andre (Organisation und Freizeitgestaltung) dazurufen mittels `@andre.prater`

## Einsteiger

### Eigene Aufgaben finden und Fortschritt dokumentieren

![Video](/docs/Filtern_dokumentieren.mp4)

kleine Zusammenfassung:

- Filtern geht über die Suchleiste beim Arbeitsbrett (`label:~"Name"`, `label:~"priorität"` )
- Aufgaben werden, in Abhängigkeit von ihrem Bearbeitungsstatus, in `To Do`, `Doing` oder `Blocked` unterteilt und bewegt
- Fortschritt (alles was ihr zum Erfüllen der Aufgabe geleistet habt) mit Kommentaren dokumentieren.
    - vorher den eigene Namen schreiben (`~"Name"`)
    - Nachrichten zum Dokumentieren können ganzen Sätze sein oder Stichpunkte sein
    - ansonsten ist es sehr frei... probiert rum ;-)
- Es besteht die Möglichkeit Kommentare im Nachhinein zu ändern oder zu löschen

### Mit anderen zusammenarbeiten

![Video](/docs/Zusammenarbeit.mp4)

kleine Zusammenfassung:
- anzeigen, dass man jetzt nicht weiterarbeiten kann
  - Kommentar, was das Problem ist (Grund mit reinschreiben)
  - mit `/label ~Blocked` anzeigen, dass die Aufgabe blockiert ist
  - bisherigen Status entfernen  `/unlabel ~Doing` oder `/unlabel ~"To Do"`
  - mit `/label ~"Person"` anzeigen, welchen Personen benötigt werden, um weiterzuarbeiten
- wurde das Problem gelöst
  - Kommentar, wie das Problem gelöst wurde (von dem, der es gelöst hat)
  - `Blocked` Label entfernen (mittels `/unlabel ~Blocked` 
  - neuen Statuslabel zuordnen mittels `/label ~Doing` oder `/label ~"To Do"`
  - mit `/unlabel ~"Person"` alle Personen entfernen, die nicht mehr benötigt werden
- Kann auch über Aufgabenbrett erledigt werden!


